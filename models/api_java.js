var db=require('../dbconnection'); //reference of dbconnection.js
 
var api={
 
getAllApi:function(callback){
 
return db.query("Select * from api",callback);
 
},
 getApiById:function(id,callback){
 
return db.query("select * from api where id=?",[id],callback);
 },
 addApi:function(api,callback){
 return db.query("Insert into api values(?,?)",[api.id,api.nama],callback);
 },
 deleteApi:function(id,callback){
  return db.query("delete from api where id=?",[id],callback);
 },
 updateApi:function(id,api,callback){
  return db.query("update api set nama=? where id=?",[api.nama,id],callback);
 }
 
};
 module.exports=api;